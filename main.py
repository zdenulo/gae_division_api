# -*- coding: utf-8  -*-


import webapp2
import json

from google.appengine.ext import ndb
from google.appengine.api import memcache


class DivisionResult(ndb.Model):
    """Kind representing result of division"""

    divisor = ndb.IntegerProperty()
    dividend = ndb.IntegerProperty()
    json_result = ndb.JsonProperty()


def str_to_integer(int_str):
    """function for converting string integer to integer, if string is not integer, it returns None"""

    try:
        no = int(int_str)
        return no
    except ValueError:
        return None


def divide(dividend, divisor):
    """
    function for doing division between integers, input are integers
    returns dictionary consisting quotient, result, modulus
    """

    if divisor == 0:
        return None
    else:
        quotient = dividend * 1.0 / divisor
        modulus = dividend % divisor
        result = {
            'quotient': quotient,
            'result': int(quotient),
            'modulus': modulus
        }
    return result


class DivisionHandler(webapp2.RequestHandler):
    """Handles requests for division, return result in json format"""

    def get(self, divisor_str):
        dividend_str = self.request.get('dividend', '')

        divisor = str_to_integer(divisor_str)
        dividend = str_to_integer(dividend_str)

        error_messages = {}
        if divisor is None:
            error_messages['error_divisor_not_int'] = "divisor is not integer, it's: " + divisor_str
        if dividend is None:
            error_messages['error_dividend_not_int'] = "dividend is not integer, it's: " + dividend_str

        if not error_messages:
            # try to get result from memcache
            key = str(dividend) + '-' + str(divisor)
            result = memcache.get(key)
            if not result:  # try to get result from DB
                db_key = ndb.Key('DivisionResult', key)
                dr = db_key.get()
                if not dr:
                    result = divide(dividend, divisor)
                    if result:  # correct calculation, save data
                        json_res = json.dumps(result)
                        memcache.set(key=key, value=json_res)
                        dr = DivisionResult(key=db_key, divisor=divisor, dividend=dividend, json_result=json_res)
                        dr.put()
                    else:
                        error_messages["divisor_zero"] = "Divisor has value 0. Can't do division"
                        json_res = json.dumps(error_messages)
                else:
                    json_res = dr.json_result
            else:
                json_res = result
        else:
            self.response.set_status(400)
            json_res = json.dumps(error_messages)
        self.response.write(json_res)


app = webapp2.WSGIApplication([
                                  ('/divide/(\w+)/', DivisionHandler)
                              ], debug=True)
