import unittest
from main import divide
from main import str_to_integer


class TestDivision(unittest.TestCase):
    def test_division_result(self):
        res = divide(10, 4)
        self.assertEqual(res['quotient'], 2.5)
        self.assertEqual(res['result'], 2)
        self.assertEqual(res['modulus'], 2)

        res = divide(10, 5)
        self.assertEqual(res['quotient'], 2.0)
        self.assertEqual(res['result'], 2)
        self.assertEqual(res['modulus'], 0)

    def test_divisor_zero(self):
        res = divide(100, 0)
        self.assertIsNone(res)

    def test_str_to_integer(self):
        x = str_to_integer("123")
        self.assertEqual(x, 123)
        x = str_to_integer("")
        self.assertIsNone(x)

        x = str_to_integer("asda")
        self.assertIsNone(x)

        x = str_to_integer("123.4")
        self.assertIsNone(x)
